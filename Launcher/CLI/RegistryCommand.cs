using CommandLine;
using Launcher.Utils;
using Serilog;
using System;
using System.IO;

namespace Launcher.CLI.Commands
{
    [Verb("reg", HelpText = "Manage game registry")]
    public class RegistryCommand
    {
        public enum Action
        {
            ADD,
            REMOVE,
            LIST
        }

        [Value(0, MetaName = "Task", HelpText = "Add, remove or list entries.", Default = Action.LIST)]
        public Action Task { get; set; }

        [Value(1, MetaName = "Target", HelpText = "Target of action. list => empty; add => game file path; remove => registry id", Required = false)]
        public string Target { get; set; }

        [Option('a', "alias", HelpText = "Alias for game client")]
        public string Alias { get; set; }

        [Option('c', "client-version", HelpText = "Version of client")]
        public int ClientVersion { get; set; }

        public void Execute()
        {
            switch(Task)
            {
                case Action.LIST:
                    ListCommand();
                    break;
                case Action.ADD:
                    AddCommand();
                    break;
                case Action.REMOVE:
                    RemoveCommand();
                    break;
            }
        }

        /// <summary>
        /// List all available clients.
        /// </summary>
        private void ListCommand()
        {
            Log.Information($"Currently {GameRegistry.GetClients().Count} clients are registered.");
            foreach(var client in GameRegistry.GetClients())
                Log.Information($"{client.Alias}:\t{client.InstallPath}");
        }

        /// <summary>
        /// Register a new client.
        /// </summary>
        private void AddCommand()
        {
            try
            {
                GameRegistry.AddClient(new GameRegistry.ClientInfo() {
                    Alias = Alias,
                    ClientVersion = ClientVersion,
                    InstallPath = Path.GetDirectoryName(Target).Replace(Path.Join("Binaries", "Win32"), ""),
                    OriginalGameFile = Path.GetFileName(Target)
                });
            } 
            catch(UserInputException ex) 
            {
                Log.Error(ex.Message);
            }
            catch(Exception ex)
            {
                Log.Fatal(ex, "Unhandled exception while adding client!");
                throw;
            }
        }

        /// <summary>
        /// Remove a client.
        /// </summary>
        private void RemoveCommand()
        {
            try 
            {
                GameRegistry.RemoveClient(c =>  c.Alias == Target);
            }
            catch(UserInputException ex)
            {
                Log.Error(ex.Message);
            }
            catch(Exception ex)
            {
                Log.Fatal(ex, "Unhandled exception while removing client!");
                throw;
            }
        }
    }
}