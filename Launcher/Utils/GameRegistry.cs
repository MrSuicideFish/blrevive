using System.IO;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System;
using Launcher.Configuration;
using Serilog;

namespace Launcher.Utils
{
    /// <summary>
    /// Manager for game clients.
    /// </summary>
    public static class GameRegistry
    {
        #region Infos
        /// <summary>
        /// Information about a unique BL:R installation.
        /// </summary>
        public class ClientInfo
        {
            /// <summary>
            /// A custom alias for better handling of multiple clients
            /// </summary>
            public string Alias { get; set; }

            /// <summary>
            /// Version of the client
            /// </summary>
            public int ClientVersion { get; set; }

            /// <summary>
            /// Path to root installation directory
            /// </summary>
            public string InstallPath { get; set; }

            /// <summary>
            /// Sub-path of binary folder
            /// </summary>
            public string BinaryDir = Path.Join("Binaries", "Win32");

            /// <summary>
            /// Filename of the original (untouched) game file
            /// </summary>
            public string OriginalGameFile = "FoxGame-win32-Shipping.exe";

            /// <summary>
            /// Filename of the patched game file
            /// </summary>
            public string PatchedGameFile = "FoxGame-win32-Shipping-Patched.exe";

            public override string ToString()
            {
                return $"{Alias} : {InstallPath}";
            }

            /// <summary>
            /// Validate the client infos.
            /// </summary>
            /// <param name="checkForPatched">whether patched file must exist</param>
            public void Validate(bool checkForPatched = false)
            {
                if(String.IsNullOrWhiteSpace(Alias))
                    throw new UserInputException("Alias must be specified");

                if(!Directory.Exists(InstallPath))
                    throw new UserInputException($"Install path ({InstallPath})does not exist!");
                
                if(!Directory.Exists(Path.Join(InstallPath, BinaryDir)))
                    throw new UserInputException("Binary directory does not exist!");
                
                if(!File.Exists(Path.Join(InstallPath, BinaryDir, OriginalGameFile)))
                    throw new UserInputException("Game file does not exist!");
                
                if(checkForPatched && !File.Exists(Path.Join(InstallPath, BinaryDir, PatchedGameFile)))
                    throw new UserInputException("Patched game file does not exist");
            }

            /// <summary>
            /// Wrapper for validate to catch UserInputExceptions.
            /// </summary>
            /// <param name="checkForPatched">whether patched file must exist</param>
            /// <param name="onError">action to take when error occurs</param>
            /// <returns>validation succeeded</returns>
            public bool TryValidate(bool checkForPatched = false, Action<UserInputException> onError = null)
            {
                try { Validate(); } catch (UserInputException ex) { onError(ex); return false; };
                return true;
            }
        }

        /// <summary>
        /// Information about a unique BL:R server instance.
        /// </summary>
        public class InstanceInfo
        {
            /// <summary>
            /// Custom alias for better handling of multiple instances
            /// </summary>
            public string Alias { get; set; }

            /// <summary>
            /// Process ID of instance
            /// </summary>
            public int InstanceProcessID { get; set; }

            /// <summary>
            /// The client used to start this instance
            /// </summary>
            public ClientInfo Client { get; set; }

            /// <summary>
            /// Validate instance info.
            /// </summary>
            public void Validate()
            {
                try {
                    Process instance = Process.GetProcessById(InstanceProcessID);
                } catch(Exception) {
                    throw new UserInputException("Process does not exist anymore");
                }
            }

            /// <summary>
            /// Wrapper for validate to catch UserInputExceptions.
            /// </summary>
            /// <param name="OnError">action to take when error occured</param>
            /// <returns>validation succeeded</returns>
            public bool TryValidate(Action<UserInputException> OnError)
            {
                try { Validate(); } catch(UserInputException ex) { OnError(ex); return false; }
                return true;
            }
        }
        #endregion

        /// <summary>
        /// list of all known clients
        /// </summary>
        private static List<ClientInfo> clients = new List<ClientInfo>();

        /// <summary>
        /// list of all known instances
        /// </summary>
        /// <typeparam name="InstanceInfo"></typeparam>
        private static List<InstanceInfo> instances = new List<InstanceInfo>();
            
        /// <summary>
        /// Initializes the registry
        /// </summary>
        public static void Initialize()
        {
            Load();
        }

        /// <summary>
        /// Load clients and instances from configuration.
        /// </summary>
        public static void Load()
        {
            // load client infos from config and validate them
            var tempClients = Config.Registry.Clients;
            clients = tempClients.Where(c => 
                c.TryValidate(false, err => { 
                    Log.Error($"Failed to parse client info: {err.Message}"); 
                })).ToList();
            
            // load instance infos from config and validate them
            var tempInstances = Config.Registry.Instances;
            instances = tempInstances.Where(i => 
                i.TryValidate( err => {
                    Log.Error($"Failed to parse instance info: {err.Message}"); 
                })).ToList();
        }

        /// <summary>
        /// Save clients and instances to configuration.
        /// </summary>
        public static void Save()
        {
            Config.Registry.Clients = clients;
            Config.Registry.Instances = instances;
            Config.Save(Config.Registry);
        }

        /// <summary>
        /// Register new client.
        /// </summary>
        /// <param name="client">info</param>
        public static void AddClient(ClientInfo client)
        {
            if(!client.TryValidate( false, err => { Log.Error($"Failed to validate client: {err.Message}"); }))
                return;

            if(GetClients(c => c.Alias == client.Alias).Count() > 0)
                throw new UserInputException($"Alias {client.Alias} has already been registered.");
            if(GetClients(c => c.InstallPath == client.InstallPath).Count() > 0)
                throw new UserInputException($"Client with same install path has already been registered.");
            
            if(client.ClientVersion == 0)
            {
                throw new UserInputException("Client version must be specified!");
            }

            clients.Add(client);
            Save();
            Log.Debug("Client added to registry");
        }

        /// <summary>
        /// Get a list of all registered clients.
        /// </summary>
        /// <param name="Filter">optional filter lambda</param>
        /// <returns>filtered list of clients</returns>
        public static List<ClientInfo> GetClients(Func<ClientInfo, bool> Filter = null)
        {
            if(Filter == null)
                return clients;
            return clients.Where(Filter).ToList();
        }

        /// <summary>
        /// Get a specific client by filter. Throws exception if multiple or none found.
        /// </summary>
        /// <param name="Filter">client filter</param>
        /// <returns>client info</returns>
        public static ClientInfo GetClient(Func<ClientInfo, bool> Filter)
        {
            var matches = GetClients(Filter);
            if(matches.Count() != 1)
                throw new UserInputException($"A client with the specified filter is not registered!");
            
            return matches.First();
        }

        /// <summary>
        /// Remove a specific client. Throws exception if multiple or none found.
        /// </summary>
        /// <param name="Filter">client filter</param>
        public static void RemoveClient(Func<ClientInfo, bool> Filter)
        {
            var client = GetClient(Filter);
            clients.Remove(client);
            Save();
            Log.Debug("Removed client from registry");
        }

        /// <summary>
        /// Get a list of instances.
        /// </summary>
        /// <param name="Filter">instance filter</param>
        /// <returns>filtered instances</returns>
        public static IEnumerable<InstanceInfo> GetInstances(Func<InstanceInfo, bool> Filter = null)
        {
            if(Filter == null)
                return instances;
            
            return instances.Where(Filter);
        }

        /// <summary>
        /// Get a specific instance. Throws exception if multiple or none are found.
        /// </summary>
        /// <param name="Filter"></param>
        /// <returns></returns>
        public static InstanceInfo GetInstance(Func<InstanceInfo, bool> Filter)
        {
            var matches = GetInstances(Filter);
            if(matches.Count() != 1)
                throw new UserInputException("Failed to find single instance.");
            
            return matches.First();
        }

        /// <summary>
        /// Register new instance.
        /// </summary>
        /// <param name="info">instance info</param>
        public static void AddInstance(InstanceInfo info)
        {
            if(GetInstances(i => i.Alias == info.Alias || i.Client.InstallPath == info.Client.InstallPath).Count() > 0)
                throw new UserInputException("Instance with alias or game path already registered.");
            
            instances.Add(info);
            Save();
        }

        /// <summary>
        /// Remove instance.
        /// </summary>
        /// <param name="Filter">instance filter</param>
        public static void RemoveInstance(Func<InstanceInfo, bool> Filter)
        {
            var instance = GetInstance(Filter);
            instances.Remove(instance);
            Save();
        }
    }
}