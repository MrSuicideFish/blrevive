using Launcher.Utils;
using System.Collections.Generic;

namespace Launcher.Configuration
{
    /// <summary>
    /// Container for GameRegistry
    /// </summary>
    public class GameRegistryProvider : IConfigProvider
    {
        public static string FileName = "GameRegistry.json";

        /// <summary>
        /// list of registered clients
        /// </summary>
        public List<GameRegistry.ClientInfo> Clients { get; set; }

        /// <summary>
        /// list of known client instances
        /// </summary>
        public List<GameRegistry.InstanceInfo> Instances { get; set; }
    }
}