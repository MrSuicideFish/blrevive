﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Launcher.Configuration
{
    /// <summary>
    /// App-level configuration.
    /// </summary>
    public class AppConfigProvider : IConfigProvider
    {
        /// <summary>
        /// The log level to use
        /// </summary>
        public int LogLevel { get; set; }
    }
}
