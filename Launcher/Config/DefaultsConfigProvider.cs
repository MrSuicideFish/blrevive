﻿using System;   
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Configuration
{
    /// <summary>
    /// Provides constant default values.
    /// </summary>
    public class DefaultConfigProvider
    {
        /// <summary>
        /// default playername
        /// </summary>
        public string PlayerName { get { return "Player"; } }

        /// <summary>
        /// default server
        /// </summary>
        /// <value></value>
        public Server LocalHostServer
        {
            get
            {
                return new Server("127.0.0.1", 7777, false);
            }
        }

    }
}
