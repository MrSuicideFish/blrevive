﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Launcher.Configuration
{
    /// <summary>
    /// Base class for config providers.
    /// </summary>
    public abstract class IConfigProvider
    {
        protected virtual bool Validate()
        {
            return true;
        }
    }
}
