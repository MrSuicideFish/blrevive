using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Runtime.CompilerServices;
using Launcher.Configuration;
using Launcher.Utils;
using System.ComponentModel;

namespace Launcher.UI
{
    public class LauncherViewDataProvider : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")  
        {  
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public int DefaultClient = 1;
        public GameRegistry.ClientInfo[] Clients
        {
            get { return Config.Registry.Clients.ToArray(); }
        }

        public GameRegistry.InstanceInfo[] Instances
        {
            get { return Config.Registry.Instances.ToArray(); }
        }

        public string[] Maps { get { return Config.Game.Maps; } }
        public string[] GameModes { get { return Config.Game.Gamemodes; } }
        public string[] Playlists { get { return Config.Game.Playlists; } }

        public string UserName { get { return Config.User.Username; } set { Config.User.Username = value; Config.Save(); }}
        public Server[] LobbyList
        {
            get { return Config.ServerList.Hosts.ToArray(); }
        }

        public int SelectedLobby = 0;
        public Server DefaultLobby { get { return Config.ServerList.PreviousHost; } set { Config.ServerList.PreviousHost = value; Config.Save(); } }


        public Server DefaultServer 
        {
            get { return Config.Defaults.LocalHostServer; }
        }
        public LauncherViewDataProvider()
        {
        }

        public static void UpdateWindow(Avalonia.Controls.IControl controls)
        {
            var p = controls.Parent;
            while(p.GetType() != typeof(Avalonia.Controls.Window))
            {
                if(p.Parent == null)
                    break;
                p = p.Parent;
            }
            
            p.DataContext = new LauncherViewDataProvider();
        }
    }
}