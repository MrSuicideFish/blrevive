using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;

namespace Launcher.UI.Controls
{
    /// <summary>
    /// Wraps a control with a label
    /// </summary>
    public class LabeledInput : ContentControl
    {
        public static readonly StyledProperty<string> LabelProperty = AvaloniaProperty.Register<LabeledInput,string>(nameof(Label));

        public string Label 
        {
            get { return GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public LabeledInput()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}