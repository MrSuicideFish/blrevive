using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using Launcher.Utils;
using System;
using System.IO;

namespace Launcher.UI.Tabs
{
    public class PatcherTab : UserControl
    {
        public PatcherTab()
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnPatchClick(object sender, RoutedEventArgs e)
        {
            var ClientComboBox = this.Find<ComboBox>("ClientList");
            var CustomInputTextBox = this.Find<TextBox>("CustomInput");
            var CustomOutputTextBox = this.Find<TextBox>("CustomOutput");

            var InjectProxyCheckBox = this.Find<CheckBox>("InjectProxy");
            var ApplyPatchesCheckBox = this.Find<CheckBox>("ApplyFixes");
            

            try
            {
                if (ClientComboBox.ItemCount == 0)
                    throw new UserInputException("Please select an client!");
                GameRegistry.ClientInfo client = (GameRegistry.ClientInfo)ClientComboBox.SelectedItem;

                Patcher.PatchGameFile(client, (bool)ApplyPatchesCheckBox.IsChecked, (bool)InjectProxyCheckBox.IsChecked, () => {
                    MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Patcher", "Patching succeeded! Goto the Client tab and connect to a server :)").Show();
                });
            } catch(UserInputException ex)
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow("Error", ex.Message, MessageBox.Avalonia.Enums.ButtonEnum.Ok).Show();
            }
        }
    }
}