using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.Interactivity;
using System;
using System.Linq;
using System.IO;
using Launcher.Utils;

namespace Launcher.UI
{
    public class RegisterClientWindow : Window
    {
        public RegisterClientWindow()
        {
            InitializeComponent();
            #if DEBUG
            this.AttachDevTools();
            #endif
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public void OnSaveClick(object sender, RoutedEventArgs e)
        {
            var PathTextBox = this.Find<TextBox>("ClientFullPath");
            var AliasTextBox = this.Find<TextBox>("ClientAlias");
            var VersionNumeric = this.Find<NumericUpDown>("ClientVersion");

            try 
            {
                if(String.IsNullOrEmpty(PathTextBox.Text))
                    throw new UserInputException("Path must be specified!");

                GameRegistry.AddClient( new GameRegistry.ClientInfo() {
                    InstallPath = Path.GetDirectoryName(PathTextBox.Text).Replace(Path.Join("Binaries", "Win32"), ""),
                    ClientVersion = (int)VersionNumeric.Value,
                    Alias = AliasTextBox.Text,
                    OriginalGameFile = Path.GetFileName(PathTextBox.Text)
                });
                this.Close();
            } 
            catch(UserInputException ex) 
            {
                MessageBox.Avalonia.MessageBoxManager.GetMessageBoxStandardWindow(
                    "Error",
                    ex.Message
                ).Show();
            }
        }

        public async void OnBrowseClick(object sender, RoutedEventArgs e)
        {
            OpenFileDialog browser = new OpenFileDialog();
            var result = await browser.ShowAsync(this);
            if(result != null)
            {
                this.Find<TextBox>("ClientFullPath").Text = result[0];
            }
        }
    }
}